'use strict';
let path = require('path');
let bodyParser = require('body-parser');
let defaultSettings = require('./defaults');
const votes   = require('../src/models/Vote');

const mongoose = require('mongoose'); // Replace with import as desired
const mongoConnectString = 'mongodb://localhost/wikipokedb';

mongoose.connect(mongoConnectString, (err) => {
  if (err) {
    console.log('Err, could not connect to the database.');
  }
});

// Additional npm or bower modules to include in builds
// Add all foreign plugins you may need into this array
// @example:
// let npmBase = path.join(__dirname, '../node_modules');
// let additionalPaths = [ path.join(npmBase, 'react-bootstrap') ];
let additionalPaths = [];

module.exports = {
  additionalPaths: additionalPaths,
  port: defaultSettings.port,
  debug: true,
  devtool: 'eval',
  output: {
    path: path.join(__dirname, '/../dist/assets'),
    filename: 'app.js',
    publicPath: defaultSettings.publicPath
  },
  devServer: {
    contentBase: './src/',
    historyApiFallback: true,
    hot: true,
    port: defaultSettings.port,
    publicPath: defaultSettings.publicPath,
    noInfo: false,
    setup: function(app) {
      app.use(bodyParser.json());

      app.post('/api/vote/',              votes.createVote);
      app.get('/api/vote/:pokemonName',   votes.getVotesByPokemonName);
      app.post('/api/updateVote/:id/',    votes.updateVote);
    }
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      actions: `${defaultSettings.srcPath}/actions/`,
      components: `${defaultSettings.srcPath}/components/`,
      sources: `${defaultSettings.srcPath}/sources/`,
      stores: `${defaultSettings.srcPath}/stores/`,
      styles: `${defaultSettings.srcPath}/styles/`,
      config: `${defaultSettings.srcPath}/config/` + process.env.REACT_WEBPACK_ENV,
      'react/lib/ReactMount': 'react-dom/lib/ReactMount'
    }
  },
  module: {}
};
