var mongoose = require('mongoose');

// model definition
let VoteSchema = mongoose.Schema({
	id:         	String,
	userId:         String,
	pokemonName:    String,
	vote:      		Boolean,
	timestamp:    	{ type: Date, default: Date.now }
});
VoteSchema.post('init', function(vote) {
	vote.id = vote._id;
});

Vote = mongoose.model('Vote', VoteSchema);

var errorResponse = { 'error': 'An error has occurred.' };


// module functions
exports.getVotesByPokemonName = function(req, res) {
	var pokemonName = req.params.pokemonName;
	console.log('Finding votes for : ' + pokemonName);
	return Vote.find( { pokemonName : pokemonName } , function(err, vote) {
		if (err) {
			console.log(err);
			return res.status(500).send(errorResponse);
		}
		res.send(vote);
	});
};

exports.createVote = function(req, res) {
	console.log('Creating new vote: ' + JSON.stringify(req.body));
	var vote = new Vote(req.body);
	return vote.save(function(err) {
		if (err) {
			console.log(err);
			return res.status(500).send(errorResponse);
		}
		res.send(vote);
		console.log('Created new vote: ' + JSON.stringify(vote));
	});
};

exports.updateVote = function(req, res) {
	var id = req.params.id;
	console.log('Updating Vote: ' + JSON.stringify(req.body));
	return Vote.findOneAndUpdate({'_id':id}, req.body, {}, function(err, vote) {
		if (err) {
			console.log(err);
			return res.status(500).send(errorResponse);
		}
		res.send(vote);
	});
};