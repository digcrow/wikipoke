import { Router, Route, browserHistory, IndexRoute  } from 'react-router';
import React from 'react';
import Main from './components/Main';

const routes = (
	<Router history = {browserHistory}>
		<Route path = "/" component = {Main}>
			<IndexRoute component = {Main} />
			<Route path = "/:name" component = {Main} />
			<Route path='*' component={ Main } />
		</Route>
	</Router>
);

export default routes;