import 'core-js/fn/object/assign';
import ReactDOM 		from 'react-dom';
import routes 			from './routes.js';

import 'normalize.css/normalize.css';

// Render the main component into the dom
ReactDOM.render(
	routes,
	document.getElementById('app')
);
