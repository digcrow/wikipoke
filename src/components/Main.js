import React from 'react';
import axios from 'axios';
import SearchInput from './SearchInput';
import PokemonCard from './PokemonCard';
import Loading from './Loading';
import PokemonDetailModal from './PokemonDetailModal';

import 'styles/App.scss';

class Main extends React.Component {

    constructor () {
		super();
		this.state = {
			isModalOpen : false,
			activePokemonName: '',
			pokemonsData : [],
			searchString : '',
			activePokemonURL : null,
			totalPokemon : 0,
			fetched : false,
			haveBookmarkedPokemons : false,
			bookmarkedPokemons: []
		}
	}

    componentWillMount() {
    	let _mainResource = 'http://pokeapi.co/api/v2/pokemon/?limit=151';
    	let _this = this;

    	axios.get(_mainResource)
	    	.then(function (response) {
				_this.setState({
					pokemonsData: response.data.results,
					totalPokemon: 151,
					fetched: true
	    		});

				let path = _this.props.params.name;
				if (!path)
					return false;
				_this.openModal(path);

			})
			.catch(function (error) {
			    console.log(error);
			});
		this.showBookmarkedPokemons();

		// ADD User ID to localStorage
		let userId = JSON.parse(localStorage.getItem('user-id')) || [];
		if(userId.length <= 0){
			let ID = Date.now()+Math.random(30).toString();
			console.log('New User creating with ID ->', ID);
			localStorage.setItem('user-id', ID);
		}
    }

	showBookmarkedPokemons(){
		let pokemons = JSON.parse(localStorage.getItem('pokemons')) || [];

		if(pokemons.length <= 0){
			this.setState({
				haveBookmarkedPokemons: false,
				bookmarkedPokemons : null
			});
			return false;
		}

		this.setState({
			haveBookmarkedPokemons: true,
			bookmarkedPokemons : pokemons
		});
	}

	openModal(name) {
        this.setState({
        	isModalOpen: true,
        	activePokemonName : this.state.pokemonsData.filter(pokemon => pokemon.name === name)[0].name
        });
		this.addClass(document.documentElement,'modal-active');
    }

    closeModal() {
        this.setState({ isModalOpen: false });
		this.removeClass(document.documentElement,'modal-active')
    }

	hasClass(el, className) {
		if (el.classList)
			return el.classList.contains(className)
		else
			return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
	}

	addClass(el, className) {
		if (el.classList)
			el.classList.add(className);
		else if (!this.hasClass(el, className)) el.className += ' ' + className
	}

	removeClass(el, className) {
		if (el.classList)
			el.classList.remove(className);
		else if (this.hasClass(el, className)) {
			var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
			el.className=el.className.replace(reg, ' ')
		}
	}

    searchByName(e){
    	let keyword = e.target.value.trim().toLowerCase();
    	this.setState({
    		searchString: keyword
    	})
    }

    getPokemonIdFromURL(url){
    	let lastSegment = url.substr( url.indexOf('pokemon/') + 8);
    	return lastSegment.substr( 0 , lastSegment.length - 1);
    }

    render(){

    	let totalPokemon = this.state.totalPokemon;
    	let pokemons = this.state.pokemonsData;
    	let searchString = this.state.searchString;

    	if( searchString.length > 0){
    		pokemons = pokemons.filter( pokemon => pokemon.name.toLowerCase().match(searchString) )
    	}

        return (
        	<div>
	            <header>
	            	<img src="/images/app-logo.png" className="app-logo" alt="WikiPokem - Gotta find them" />
	            </header>

	        	<div className="container">

	        	    <SearchInput searchString={ this.state.searchString }
	        	    		 	 searchByName={ this.searchByName.bind(this) } />
	        		<p className="search-input-info"> Search for the pokemon you want to know more about and click for more details. We know all about { totalPokemon }  Pokémons and we take care of them. </p>
	        		
	            </div>
				{
					( this.state.haveBookmarkedPokemons ) ?
						<div className="container">
							<h2>Bookmarked Pokemons :</h2>
							<div className=" container pokemons-list bookmarked-list">
								{this.state.bookmarkedPokemons.map((pokemon) => (
									<PokemonCard pokemonName={pokemon.name}
												 pokemonID={this.getPokemonIdFromURL(pokemon.name)}
												 key={pokemon.name + '-saved'}
												 onClick={ () =>  this.openModal(pokemon.name) } />
								))}
							</div>
						</div>
					: ''
				}


	        	<div className="container pokemons-list">
	        		{
	        			( this.state.fetched ) ?
			            	pokemons.map((pokemon) => (
									<PokemonCard pokemonName={pokemon.name}
												 pokemonID={this.getPokemonIdFromURL(pokemon.url)}
												 key={pokemon.name}
												 onClick={ () =>  this.openModal(pokemon.name) }/>
							))
			            :
			            	<Loading message="Please wait while we are weaking up the pokemons ..." />
		            }
		        </div>
		        <PokemonDetailModal isModalOpen={this.state.isModalOpen}
                			   		closeModal={this.closeModal.bind(this) }
                			   		activePokemonName={ this.state.activePokemonName }
                			   		transitionName="modal-anim"
									updateBookmarkedPokemons={ this.showBookmarkedPokemons.bind(this) }/>
	        </div>
        );
    }
}

export default Main;
