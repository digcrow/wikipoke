import React from 'react';
import axios from 'axios';
import 'styles/VotePokemon.scss';

class VotePokemon extends React.Component {

    constructor(){
        super();
        this.state = {
            likes    : 0,
            dislikes : 0,
            voteCount:0,
            ownVote  : null,
            voteId  : null,
            userId   : JSON.parse(localStorage.getItem('user-id'))
        }
        this.loading = false;
    }

    componentWillMount(){
        let _this = this;

        axios.get('api/vote/'+this.props.name)
            .then(function(response){
                _this.updateState(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    updateState(votes){
        let likes = 0;
        let dislikes = 0;
        let ownVote = null;
        let voteId = null;
        votes.map((vote) => {
            if (vote.vote === true)
                likes ++;
            else
                dislikes ++;
            if( vote.userId == String (this.state.userId)){
                ownVote = vote.vote;
                voteId  = vote._id;
            }

        });

        this.setState({
            likes    : likes,
            dislikes : dislikes,
            voteCount:votes.length,
            ownVote  : ownVote,
            voteId   : voteId
        });
    }


    VoteForPokemon(pokemonName,vote){

        if(this.loading)
            return;
        this.loading = true;

        let _this = this;

        if( this.state.ownVote == vote) {
            this.loading = false;
            return;
        }

        if( this.state.ownVote === null ){
            this.setState({
                likes: (vote == 1 ) ? this.state.likes + 1 : this.state.likes ,
                dislikes: (vote == 0 ) ? this.state.dislikes + 1 : this.state.dislikes ,
                ownVote: vote,
                voteCount : this.state.voteCount + 1
            });
            axios({
                method: 'post',
                url: 'api/vote/',
                data: {
                    userId : this.state.userId,
                    pokemonName: pokemonName,
                    vote : vote
                }
            })
                .then(function(response){
                    _this.setState({
                        voteId : response.data._id
                    })
                    _this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        if( this.state.ownVote != null && this.state.ownVote != vote ) {
            this.setState({
                likes: (vote == 1 ) ? this.state.likes + 1 : this.state.likes - 1 ,
                dislikes: (vote == 0 ) ? this.state.dislikes + 1 : this.state.dislikes - 1 ,
                ownVote: vote
            });
            axios.post('api/updateVote/'+this.state.voteId,{
                    vote : vote
                })
                .then(function(response){
                    _this.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }

    render(){

        return(
            <div className="vote-wrapper">
                <h4 className="Vote-detail-wrapper">
                    <span className="vote-count">{ this.state.voteCount } </span> Vote{(this.state.voteCount > 1)? 's' : '' } for <span className="pokemon-name">{this.props.name}</span>. <br/>
                    <small className="vote-detail"> {this.state.likes} <i className="fa fa-thumbs-up like"></i>  {this.state.dislikes} <i className="fa fa-thumbs-down dislike"></i></small>
                </h4>
                <button
                    className={(this.state.ownVote == 1 )? 'vote-btn like active': 'vote-btn like'}
                    onClick={ () => this.VoteForPokemon(this.props.name,1) }>
                    <i className="fa fa-thumbs-up"></i> Like
                </button>
                <button
                    className={(this.state.ownVote == 0 )? 'vote-btn dislike active': 'vote-btn dislike'}
                    onClick={ () => this.VoteForPokemon(this.props.name,0) }>
                    <i className="fa fa-thumbs-down"></i> Dislike
                </button>
            </div>
        )
    }
}

export default VotePokemon;