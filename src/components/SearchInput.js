import React from 'react';
import 'styles/Loading.scss';

const SearchInput = (props) =>
  	<input  type="text"
			className="search-input"
			placeholder="Search for pokemons..."
			value={ props.searchString }
			onChange={ props.searchByName } />

export default SearchInput;
