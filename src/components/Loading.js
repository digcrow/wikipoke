import React from 'react';
import 'styles/Loading.scss';

const Loading = (props) =>
  	<div  className="loader">
		<img src="/images/loading.gif" alt="Loading..." className="loading-anim" />
		<p className="message">
			{ props.message || 'Please wait...' }
		</p>
	</div>

export default Loading;
