import React                from 'react';
import axios                from 'axios';
import { Bar }              from 'react-chartjs-2';


class PokemonStats extends React.Component {

    constructor(){
        super();
        this.state = {
            averageStats : {},
            chartData : {}
        };
    }

    componentWillMount(){

        let _mainResource = '/data/average_base_stats.json';
        let _this = this;

        axios.get(_mainResource)
            .then(function (response) {
                _this.state.averageStats = response.data;
                _this.state.chartData = {
                    labels: ['HP', 'Attack', 'Defense', 'Special Attack', 'Special Defense', 'Speed'],
                    datasets: _this.getPokemonBaseStatsChartData(_this.props.pokemon)
                };
                _this.forceUpdate();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getPokemonBaseStats(arg, sameType = false){
        if(!sameType)
            return [
                arg.hp,
                arg.attack,
                arg.defense,
                arg.sp_atk,
                arg.sp_def,
                arg.speed
            ];

        return [
            this.state.averageStats[arg.name].hp,
            this.state.averageStats[arg.name].attack,
            this.state.averageStats[arg.name].defence,
            this.state.averageStats[arg.name].sp_atk,
            this.state.averageStats[arg.name].sp_def,
            this.state.averageStats[arg.name].speed
        ];
    }
    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    getPokemonBaseStatsChartData(pokemon){

        let data = [];
        let _this = this;
        data.push({
            label: pokemon.name + ' Base Stats',
            backgroundColor: [
                'rgba(255, 206, 86, 0.3)',
                'rgba(255, 99, 132, 0.3)',
                'rgba(54, 162, 235, 0.3)',
                'rgba(75, 192, 192, 0.3)',
                'rgba(153, 102, 255, 0.3)',
                'rgba(255, 159, 64, 0.3)'
            ],
            borderColor: [
                'rgba(255, 206, 86, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            data: _this.getPokemonBaseStats(pokemon)
        });
        pokemon.types.map((type, index) =>{
            let alpha = 0.18 +(index/10);
            data.push({
                label: _this.capitalizeFirstLetter( type.name ) + ' Average Base Stats',
                backgroundColor: [
                    'rgba(0, 0, 0, '+ alpha +')',
                    'rgba(0, 0, 0, '+ alpha +')',
                    'rgba(0, 0, 0, '+ alpha +')',
                    'rgba(0, 0, 0, '+ alpha +')',
                    'rgba(0, 0, 0, '+ alpha +')',
                    'rgba(0, 0, 0, '+ alpha +')'
                ],
                borderColor: [
                    'rgba(0, 0, 0, 0.2)',
                    'rgba(0, 0, 0, 0.2)',
                    'rgba(0, 0, 0, 0.2)',
                    'rgba(0, 0, 0, 0.2)',
                    'rgba(0, 0, 0, 0.2)',
                    'rgba(0, 0, 0, 0.2)'
                ],
                borderWidth: 1,
                data: _this.getPokemonBaseStats(type,true)
            })
        });
        return data
    }

    render(){

        let chartOption = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        min : 10
                    }
                }]
            }
        };

        return <Bar data={this.state.chartData} height={250} options={chartOption} />
    }
}

export default PokemonStats;