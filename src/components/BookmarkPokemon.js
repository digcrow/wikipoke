import React from 'react';
import 'styles/BookmarkPokemon.scss';

class BookmarkPokemon extends React.Component {

    bookmarkThisPokemon(pokemonName){

        let pokemons = JSON.parse(localStorage.getItem('pokemons')) || [];
        let index = pokemons.findIndex(pk => pk.name === pokemonName);

        if( index != -1 ){
            pokemons.splice(index, 1);
            localStorage.setItem('pokemons', JSON.stringify(pokemons));
        }else{
            pokemons.push({ name: pokemonName });
            localStorage.setItem('pokemons', JSON.stringify(pokemons));
        }

        this.props.updateBookmarkedPokemons();

    }

    render(){

        let bookmarkedPokemons = JSON.parse(localStorage.getItem('pokemons')) || [];
        let isBookmarked = bookmarkedPokemons.filter(pk => pk.name === this.props.name ).length;

        return(
            ( ! isBookmarked ) ?
                <button
                    className="bookmark-btn save"
                    onClick={ () => this.bookmarkThisPokemon(this.props.name) }>
                    <i className="fa fa-bookmark"></i> Save for later
                </button>
                :
                <button
                    className="bookmark-btn unsave"
                    onClick={ () => this.bookmarkThisPokemon(this.props.name) } >
                    <i className="fa fa-trash"></i> Remove from bookmark
                </button>
        )
    }
}

export default BookmarkPokemon;