import React                from 'react';
import { Link }             from 'react-router';
import axios                from 'axios';
import CSSTransitionGroup   from 'react-addons-css-transition-group';
import PokemonDescription   from './PokemonDescription';
import PokemonStats         from './PokemonStats';
import PokemonImage         from './PokemonImage';
import BookmarkPokemon      from './BookmarkPokemon';
import VotePokemon      	from './VotePokemon';
import LabelType            from './LabelType';
import Loading              from './Loading';

let Twitter                 = require('twitter-node-client').Twitter;
import                      'styles/PokemonDetail.scss';

class PokemonDetail extends React.Component {

    componentWillMount(){

        // Get Tweets by HashTag
        let twitter = new Twitter({
            'consumerKey': 'zoe3xtMi1wSUIDkn9QJOLVbkU',
            'consumerSecret': 'Muwcz7q85Wl8NFRcI9XaGZph1z4n3U1I0NSWCn3tsVcKUbwmPK',
            'accessToken': '69025683-kQ6l1HXci4QfJ43cTim2aGrNdl4VsgzaCSoTgKyeL',
            'accessTokenSecret': 'FylqCjYXYJ5syUzkOnqzRLz4YzzLEmVkIrVWVlJlhT9V1'
        });
        //twitter.getSearch({
        //        'q':'#'+this.props.pokemon.name ,
        //        'count': 10
        //    },
        //    (error)=>{
        //        console.log(error)},
        //    (success)=>{
        //        console.log(success)
        //});

    }

	render(){

		let pokemon = this.props.pokemon;

		return(
			<div>
    			<div className="modal-body clearfix">

                    <div className="col-50">
						<h2 className="name-wrapper show-mobile">
							<small>Pokemon Name</small>
							<span className="name">{pokemon.name}</span>
						</h2>
                        <div className="image-wrapper">
                            <PokemonImage name={pokemon.name} />
                            <div className="national-id"> National id : {pokemon.national_id}</div>
                        </div>
						<VotePokemon
							name={ pokemon.name.toLowerCase() } />
						<PokemonStats pokemon={pokemon} />
                    </div>

					<div className="col-50">
                        <h2 className="name-wrapper hide-mobile">
                            <small>Pokemon Name</small>
                            <span className="name">{pokemon.name}</span>
                        </h2>
                        <h4 className="detail-title"> Description : </h4>
                        <PokemonDescription url={pokemon.descriptions[0].resource_uri} />
                        <PokemonDescription url={pokemon.descriptions[1].resource_uri} />

                        <div className="pokemon-type-wrapper">
                            <h4 className="detail-title"> Pokemon Type : </h4>
                            {
                                pokemon.types.map((type) => (
                                    <LabelType type={type.name} key={type.name} />
                                ))
                            }
                        </div>

                        <div className="pokemon-type-wrapper">
                            <h4 className="detail-title"> Pokemon Abilities : </h4>
                            {
                                pokemon.abilities.map((ability) => (
                                    <div className="pokemon-ability" key={ability.name }> {ability.name } </div>
                                ))
                            }
                        </div>
                        <div className="pokemon-weight-wrapper">
                            <h4 className="detail-title"> Pokemon Weight : </h4>
                            <p className="pokemon-attr-detail">{pokemon.weight/10}Kg </p>
                        </div>
                        <div className="pokemon-height-wrapper">
                            <h4 className="detail-title"> Pokemon Height : </h4>
                            <p className="pokemon-attr-detail">0.{pokemon.height}M </p>
                        </div>
						<div className="pokemon-height-wrapper">
                            <h4 className="detail-title"> Species : </h4>
                            <p className="pokemon-attr-detail">{pokemon.species || 'Not Defined'} </p>
                        </div>
						<div className="pokemon-height-wrapper">
							<h4 className="detail-title"> Happiness : </h4>
							<p className="pokemon-attr-detail">{pokemon.happiness} </p>
						</div>

                        <BookmarkPokemon
                            name={ pokemon.name.toLowerCase() }
                            updateBookmarkedPokemons={this.props.updateBookmarkedPokemons} />

                    </div>
    			</div>
    		</div>
		)
	}
}

class PokemonDetailModal extends React.Component {

	constructor () {
		super();
		this.state = {
			fetched: false,
			pokemon: {}
		}
	}

	componentWillReceiveProps(nextProps){

		if(nextProps.activePokemonName === this.props.activePokemonName)
			return false;

		this.setState({
			fetched: false
		});

    	let _mainResource =  `http://pokeapi.co/api/v1/pokemon/${nextProps.activePokemonName}/`;
    	let _this = this;

    	axios.get(_mainResource)
	    	.then(function (response) {
				_this.setState({
					pokemon: response.data,
					fetched: true
	    		});
	    		_this.forceUpdate;
			})
			.catch(function (error) {
			    console.log(error);
			});
	}

	render(){

    	if(this.props.isModalOpen){
    		return(
	    		<CSSTransitionGroup
	    			transitionName={this.props.transitionName}
	    			transitionEnterTimeout={500}
          			transitionLeaveTimeout={300} >
          			<div className="modal-container">
          				<div className="modal">
                            <Link className="close-btn" onClick={this.props.closeModal} to="/"> &times; </Link>
          					{
          						( this.state.fetched ) ?
          							<PokemonDetail
                                        pokemon={this.state.pokemon}
                                        closeFunction={this.props.closeModal}
                                        updateBookmarkedPokemons={this.props.updateBookmarkedPokemons}
                                    />
          						:
          							<Loading message={`We are calling ${this.props.activePokemonName} ...`} />
          					}
          				</div>
			    	</div>
		    	</CSSTransitionGroup>
		    	
		    )
    	}else{
    		return(
    			<CSSTransitionGroup
    				transitionName={this.props.transitionName}
	    			transitionEnterTimeout={500}
          			transitionLeaveTimeout={300} />
    		)
    	}
    }
}

export default PokemonDetailModal;