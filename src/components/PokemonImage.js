import React from 'react';

const PokemonImage = (props) =>
  	<img src={`/images/pokemons/${props.name.replace('♀', 'f').replace('♂', 'm').replace(/\W+/g, '').toLowerCase()}.jpg`} className="pokemon-card__image"/>

export default PokemonImage;
