import React                from 'react';
import axios                from 'axios';

class PokemonDescription extends React.Component {

    constructor(){
        super();
        this.state = {
            pokemonDescription : ''
        };
    }

    componentWillMount(){
        let _mainResource = 'http://pokeapi.co/' + this.props.url;
        let _this = this;

        axios.get(_mainResource)
            .then(function (response) {
                _this.setState({
                    pokemonDescription: response.data.description
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return <p className="pokemon-description"> { this.state.pokemonDescription } </p>
    }
}

export default PokemonDescription;