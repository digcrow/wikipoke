import React from 'react';
import { Link } from 'react-router';
import PokemonImage from './PokemonImage';

import 'styles/PokemonCard.scss';

class PokemonCard extends React.Component {

	shouldComponentUpdate(nextProps){
		if(nextProps !== this.props){
			return false;
		}
	}

	render(){
		
		let { pokemonName , pokemonID } = this.props;
		return(
			<Link className="pokemon-card" onClick={this.props.onClick} to={`/${pokemonName}`}>
				<div className="pokemon-card__image-wrapper">
					<div className="pokemon-card__label">#{ pokemonID } </div>
					<PokemonImage name={pokemonName} />
				</div>
				<div className="pokemon-card__name">
					<h3 className="pokemon-card__pokemon-name">{ pokemonName }</h3>
				</div>
				<div className="pokemon-card__link">
					<button>More info</button>
				</div>
			</Link>
		)
	}
}

export default PokemonCard;