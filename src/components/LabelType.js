import React from 'react';
import 'styles/LabelType.scss';

const LabelType = (props) =>
	<div className="pokemon-type-label">
		<i className={`icn icn-${props.type}`} >
			<img src={`/images/pokemonTypes/${props.type}.png`} alt={props.type} />
		</i>
		{ props.type }
	</div>

export default LabelType;
