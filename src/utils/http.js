import axios from 'axios';

let _mainResource = 'data/pokemons.json';
let pokemons;


export async function getPokemons () {
  let resolve = await axios.get(_mainResource);
  return resolve.data;
}

export async function getPokemonByName (name) {
  pokemons = await getPokemons();
  return pokemons.filter(pokemon => pokemon.name === name)[0]
}

// export let getBookmarks = () => {
//   let bookmarkedPokemons = _localStorage.getItem(pokemons)
//   let pokemons = (!bookmarkedPokemons) ? [] : JSON.parse(bookmarkedPokemons)
//   return pokemons
// }

// export let saveBookmarks = (pokemon) => {
//   let bookmarkedPokemons = getBookmarks()
//   bookmarkedPokemons.push(pokemon)
//   _localStorage.setItem(pokemons, JSON.stringify(bookmarkedPokemons))
// }